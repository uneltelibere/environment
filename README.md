# Development environment

## What is this?

A [PostgreSQL](https://www.postgresql.org/) container and a [GreenMail](http://www.icegreen.com/greenmail/) container.

After starting them up, you'll have:

* a PostgreSQL server running on the default port `5432`
* a pgAdmin server running at http://localhost:5050
* a mail server running on default ports + `3000` (e.g. `3025` instead of `25` for SMTP).

Use them to mimic a production environment.

## Commands

To start the environment:

    ./start.sh

To stop the environment:

    ./stop.sh

To create databases:

* Copy `dbs.json.template` to `dbs.json`
* Run `create-dbs.sh`

To re-create a database with the content of a dump file, place the dump file somewhere in the `data` directory, then run:

    ./re-create-db.sh my-db-prod data/my_db.dump

To open a shell in the container, as the `postgres` user:

    docker exec -it pgdev bash -c "su postgres"

Attention: don't do `su -` because we want to keep the environment variables (like `POSTGRES_USER`).

## Usage

For logging in to pgAdmin, use the `PGADMIN_DEFAULT_EMAIL` and `PGADMIN_DEFAULT_PASSWORD` values from `docker-compose.yaml`.

To connect to the database server from pgAdmin, use the container name (`pgdev`) as hostname (`localhost` won't work) and the `POSTGRES_USER` / `POSTGRES_PASSWORD` values for authentication.
