FROM postgres:9.5
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y python-psycopg2
