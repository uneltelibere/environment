#!/bin/bash

if [ $# -ne 2 ]
then
	echo "Usage: $0 db-name dump-file"
	exit 1
fi

db_name=$1
dump_file=$2
if [[ $dump_file == /* ]];
then
	echo "The dump file should be a path relative to the current directory."
	exit 1
fi
dump_file=/home/share/$dump_file

# Not doing "su -" because we want to keep the environment variables
# (like POSTGRES_USER)

docker exec -it pgdev su -c "/home/share/tool/re-create-db.sh $db_name $dump_file" postgres

