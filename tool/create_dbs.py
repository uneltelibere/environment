#!/usr/bin/python

import collections
import json
import os
import psycopg2
import subprocess
import sys
from argparse import ArgumentParser

DbWithDetails = collections.namedtuple('DbWithDetails', ['name', 'user', 'passwd'])

def create_db(conn, db_name):
	cur = conn.cursor()
	try:
		cur.execute("""SELECT COUNT(*) FROM pg_database WHERE datname = '{}'""".format(db_name))
	except:
		print "I can't SELECT from pg_database"

	row = cur.fetchone()
	if row[0] == 1:
		print "Database {} already exists.".format(db_name)
	else:
		print "Database {} does not exist. Creating.".format(db_name)
		subprocess.call(["createdb", "-T", "template0", db_name])

def create_user(conn, db):
	cur = conn.cursor()
	try:
		cur.execute("""SELECT COUNT(*) FROM pg_user WHERE usename = '{}'""".format(db.user))
	except:
		print "I can't SELECT from pg_user"

	row = cur.fetchone()
	if row[0] == 1:
		print "User {} already exists.".format(db.user)
	else:
		print "User {} does not exist. Creating.".format(db.user)
		cur.execute("""CREATE ROLE {} LOGIN ENCRYPTED PASSWORD '{}'""".format(db.user, db.passwd))
		cur.execute("""GRANT ALL PRIVILEGES ON DATABASE {} TO {}""".format(db.name, db.user))
	conn.commit()

def create_db_and_user(conn, db_entry):
    create_db(conn, db_entry.name)
    create_user(conn, db_entry)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-n", "--name", help="database name")
    parser.add_argument("-u", "--user", help="database user")
    parser.add_argument("-p", "--password", help="database password")
    args = parser.parse_args()

    try:
	psqldb = DbWithDetails(os.environ['POSTGRES_DB'], os.environ['POSTGRES_USER'], os.environ['POSTGRES_PASSWORD'])
	print "Connecting to {}".format(psqldb)
	conn = psycopg2.connect("dbname='{}' user='{}' password='{}'".format(psqldb.name, psqldb.user, psqldb.passwd))
    except:
	print >> sys.stderr, "I am unable to connect to the database."
        sys.exit(1)

    if args.name != None and args.user != None and args.password != None:
        db_entry = DbWithDetails(args.name, args.user, args.password)
        create_db_and_user(conn, db_entry)
    else:
        with open('dbs.json') as json_data:
            settings = json.load(json_data)
            for db in settings['databases']:
                db_entry = DbWithDetails(db['name'], db['user'], db['password'])
                create_db_and_user(conn, db_entry)

