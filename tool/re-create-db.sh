#!/bin/sh

if [ $# -ne 2 ]
then
	echo "Usage: $0 db-name dump-file"
	exit 1
fi

db_name=$1
dump_file=$2

dropdb $db_name && createdb -T template0 $db_name && pg_restore -d $db_name $dump_file

