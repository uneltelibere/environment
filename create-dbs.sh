#!/bin/sh

# Not doing "su -" because we want to keep the environment variables
# (like POSTGRES_USER)

docker exec -it pgdev su -c "cd /home/share && ./tool/create_dbs.sh" postgres

